#[cfg(test)]
mod tests {
    use tide::{StatusCode};
    use tide_testing::TideTestingExt;
    use crate::{mime, order_shoes, welcome};

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn it_not_works() {
        assert_eq!(2 + 3, 5);
    }


    #[async_std::test]
    async fn is_url_orders_working() -> tide::Result<()> {
        let mut app = tide::new();
        app.at("/orders/shoes").get(order_shoes);

        let mut resp = app.get("/orders/shoes").await?;
        assert_eq!(resp.status(), StatusCode::Ok);
        assert_eq!(resp.body_string().await.unwrap(), "Hello! I've put in an order for shoes");
        Ok(())
    }


    #[async_std::test]
    async fn is_url_index_working() -> tide::Result<()> {
        let mut app = tide::new();
        app.at("/").get(welcome);

        let mut resp = app.get("/").await?;
        assert_eq!(resp.status(), StatusCode::Ok);
        assert_eq!(resp.content_type().unwrap(), mime::HTML);
        assert_eq!(resp.body_string().await.unwrap(), "<h1>Bienvenido</h1>");
        Ok(())
    }
}